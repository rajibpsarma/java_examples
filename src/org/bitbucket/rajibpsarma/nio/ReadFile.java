package org.bitbucket.rajibpsarma.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class ReadFile {
	private final static String FILE_NAME_INPUT = "D:\\temp\\Test1.pdf";
	private final static String FILE_NAME_OUTPUT = "D:\\temp\\Test2.pdf";
	public static void main(String[] args) {
		FileChannel inChannel = null, outChannel = null;
		RandomAccessFile inFile = null, outFile = null;
		try {
			// Read the input file
            inFile = new RandomAccessFile(FILE_NAME_INPUT, "r");
            inChannel = inFile.getChannel();
            MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
            buffer.load();
            
            
            // Write the data to output file
            outFile = new RandomAccessFile(FILE_NAME_OUTPUT, "rw");
            outChannel = outFile.getChannel();
            outChannel.write(buffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
		finally {
			if(inChannel != null) {
				try {
					inChannel.close();
					inFile.close();
				} catch(IOException e) {}
			}
			if(outChannel != null) {
				try {
					outChannel.close();
					outFile.close();
				} catch(IOException e) {}
			}			
		}
	}
}
