package org.bitbucket.rajibpsarma.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorTest {
	private static List<Integer> data = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		// Populate the list
		for(int i = 1; i <=5; i++)
			data.add(i);
		
		System.out.println("Before : " + data); // [1, 2, 3, 4, 5]
		
		// We want to remove 2. update 3 to 300, add 6
		ListIterator<Integer> itr = data.listIterator();
		while(itr.hasNext()) {
			int tmp = itr.next();
			System.out.println("tmp = "+ tmp);
			if(tmp == 2) {
				// remove
				itr.remove();
			}
			else if(tmp == 3) {
				// Update to 300
				itr.set(300);
			}
		}
		itr.add(6);
		
		System.out.println("After : " + data); // [1, 300, 4, 5, 6]

	}
}
