package org.bitbucket.rajibpsarma.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadTextFile {
	private final static String FILE_NAME_INPUT = "D:\\temp\\Test.txt";
	private final static String FILE_NAME_OUTPUT = "D:\\temp\\Test2.txt";
	
	public static void main(String[] args) {
		Stream<String> lines = null;
		try {
			Path fileIn = Paths.get(FILE_NAME_INPUT);
			Path fileOut = Paths.get(FILE_NAME_OUTPUT);
			
			// Read input file
			lines = Files.lines(fileIn, StandardCharsets.UTF_8 );
			
			// Write
			Files.write(fileOut, (Iterable<String>)lines::iterator);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(lines != null) {
				lines.close();
			}
		}
	}
}
