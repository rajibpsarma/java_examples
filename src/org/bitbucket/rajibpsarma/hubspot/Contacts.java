package org.bitbucket.rajibpsarma.hubspot;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * It fetches all contacts info from HubSpot.
 */
public class Contacts {
	public static void main(String[] args) {	
		// Get all the contacts using HubSpot API key authentication.
		String uri = "https://api.hubapi.com/crm/v3/objects/contacts?hapikey=xxx-yyy-zzz";
		
		HttpGet http = new HttpGet(uri);
		try(CloseableHttpResponse res = HttpClientBuilder.create().build().execute(http)) {
			int status = res.getStatusLine().getStatusCode();
			System.out.println("Status : " + status);
			if(200 == status) {
				HttpEntity entity = res.getEntity();
				System.out.println("Contact data retrieved : ");
				System.out.println(IOUtils.toString(entity.getContent(), "UTF-8"));
			}
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
