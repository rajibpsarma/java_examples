package org.bitbucket.rajibpsarma.util;

import java.util.Scanner;

/**
 * Scanner related example
 * @author RSarma
 *
 */
public class ScannerExample {
	public static void main(String[] args) {
		readInputFromConsole2();
	}
	
	/**
	 * Reads input using scanner. It reads 3 values stdin e.g. "1 2 3"
	 */
	private static void readInputFromConsole1() {
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		int b = scan.nextInt();
		int c = scan.nextInt();
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}
	
	/**
	 *  you must read an integer, a double, and a String from stdin, in 3 lines of input,
	 *  then print the values according to the instructions
	 */
	private static void readInputFromConsole2() {
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        double d = scan.nextDouble();
        //scan.next();
        //String s = scan.next("[a-z A-Z 0-9 ]");
        scan.nextLine();
        String s = scan.nextLine();

        // Write your code here.

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
	}
}
