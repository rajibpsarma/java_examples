package org.bitbucket.rajibpsarma.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsTest {
	public static void main(String[] args) {
		Box<Integer> box1 = new Box<Integer>();
		box1.set(1);
		Box<Integer> box2 = new Box<Integer>();
		box2.set(1);	
		System.out.println(box1.getArea(2.5));
		// The following generates compile time error
		// System.out.println(box1.getArea("a"));
		System.out.println("Equal : " + equals(box1, box2));
		
		List<Vehicle> list = new ArrayList<Vehicle>();
		list.add(new FourWheeler());
		list.add(new TwoWheeler());
		list.add(new FourWheeler());
		System.out.println("1. Wheels : " + calculateTotalWheels(list));
		
		List<FourWheeler> lf = new ArrayList<FourWheeler>();
		lf.add(new FourWheeler());
		lf.add(new FourWheeler());
		System.out.println("2. Wheels : " + calculateTotalWheels(lf));
		
		List<TwoWheeler> lt = new ArrayList<>();
		lt.add(new TwoWheeler());
		lt.add(new TwoWheeler());
		
		System.out.println("3. Wheels : " + calculateTotalWheelCount(list));
		// The following gives compile time error
		//System.out.println("3. Wheels : " + calculateTotalWheelCount(lf));
		System.out.println("3. Wheels : " + calculateTotalWheelCount(lt));
		
	}
	
	private static <T> boolean equals(Box<T> b1, Box<T> b2) {
		return b1.get().equals(b2.get());
	}	
	
	private static int calculateTotalWheels(List<? extends Vehicle> list) {
		int total = 0;
		for(Vehicle v : list) {
			total += v.getNumberOfWheels();
		}
		return total;
	}
	
	private static int calculateTotalWheelCount(List<? super TwoWheeler> list) {
		int total = 0;
		for(Object v : list) {
			Vehicle tmp = (Vehicle)v;
			total += tmp.getNumberOfWheels();
		}
		return total;
	}	
}


/**
 * Generic version of the Box class.
 * @param <T> the type of the value being boxed
 */
class Box<T> {
    // T stands for "Type"
    private T t;

    public void set(T t) { this.t = t; }
    public T get() { return t; }
    public <U extends Number> Number getArea(U length) {
    	return (4 * length.doubleValue());
    }
}

class Vehicle {
	private int numberOfWheels;
	public int getNumberOfWheels() {
		return this.numberOfWheels;
	}
	public Vehicle(int wheels) {
		this.numberOfWheels = wheels;
	}
}
class FourWheeler extends Vehicle {
	public FourWheeler() {
		super(4);
	}
}
class TwoWheeler extends Vehicle {
	public TwoWheeler() {
		super(2);
	}
}