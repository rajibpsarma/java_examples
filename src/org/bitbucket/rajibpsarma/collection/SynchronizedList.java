package org.bitbucket.rajibpsarma.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SynchronizedList {
	public static void main(String[] args) throws Exception {
		ThreadClient client = new ThreadClient();
		Thread1 t1 = new Thread1(client);
		Thread2 t2 = new Thread2(client);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		System.out.println(client.getData());
	}
}

class ThreadClient {
	// If we use ArrayList here, we may get error at runtime as it's used by 2 threads.
	// Also, we might get unpredictable values. 
	//private List<Integer> list = new ArrayList<Integer>();
	private List<Integer> list = new CopyOnWriteArrayList<Integer>();
	// private List<Integer> list = Collections.synchronizedList(new ArrayList<Integer>());
	public void addData(int no) {
		list.add(no);
	}
	public String getData() {
		return list.toString();
	}
}

class Thread1 extends Thread {
	private ThreadClient client;
	public Thread1(ThreadClient c) {
		client = c;
	}
	public void run() {
		for(int i = 1; i<=10; i++) {
			client.addData(i);
		}
	}
}

class Thread2 extends Thread {
	private ThreadClient client;
	public Thread2(ThreadClient c) {
		client = c;
	}
	public void run() {
		for(int i = 100; i<=110; i++) {
			client.addData(i);
		}
	}
}