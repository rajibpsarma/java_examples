package org.bitbucket.rajibpsarma.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import junit.framework.Assert;

public class RegEx1Test {
	@Test
	public void testInvalid() {
		// user name must be between 2 and 17 characters long.
		Assert.assertFalse(RegEx1.apply("a")); // Less than 2
		Assert.assertFalse(RegEx1.apply("abcdfghjklpoiuytre")); // More than 17
		
		// valid characters are A to Z, a to z, 0 to 9, . (full-stop), _ (underscore) and  (hyphen)
		Assert.assertFalse(RegEx1.apply("ab@")); // An invalid char
		
		// user name must begin with an alphabetic character.
		Assert.assertFalse(RegEx1.apply("9ab")); // An invalid char at starting
		
		// user name must not end with a . (full stop) or _ (underscore) or  (hyphen).
		Assert.assertFalse(RegEx1.apply("ab.")); // An invalid char at the end
		Assert.assertFalse(RegEx1.apply("ab_")); // An invalid char at the end
		Assert.assertFalse(RegEx1.apply("ab-")); // An invalid char at the end
	}
	
	@Test
	public void testValidValues() {
		// user name must be between 2 and 17 characters long.
		Assert.assertTrue(RegEx1.apply("ab")); // 2 chars
		Assert.assertTrue(RegEx1.apply("absdfg7hj8klo4uyt")); // 17 chars
		
		// valid characters are A to Z, a to z, 0 to 9, . (full-stop), _ (underscore) and  (hyphen)
		Assert.assertTrue(RegEx1.apply("azAZ019._-A")); // All valid chars
		
		// user name must begin with an alphabetic character.
		Assert.assertTrue(RegEx1.apply("az9"));
		Assert.assertTrue(RegEx1.apply("zz9"));
		Assert.assertTrue(RegEx1.apply("Az9"));
		Assert.assertTrue(RegEx1.apply("Zz9"));
		Assert.assertTrue(RegEx1.apply("Xz9")); 
	}
	
	public static void main(String[] args) {
		checkUserName();
	}
	
	// https://www.hackerrank.com/challenges/valid-username-checker/problem
	private static void checkUserName() {
		
	}
}

class RegEx1 {
	/*
	 The rules to be used
	-	user name must be between 2 and 17 characters long.
	-	valid characters are A to Z, a to z, 0 to 9, . (full-stop), _ (underscore) and  (hyphen)
	The reg ex of above 2 are: [a-zA-Z0-9._-]{0,15}
	-	user name must begin with an alphabetic character.
	Its reg ex is: ^[a-zA-Z]
	-	user name must not end with a . (full stop) or _ (underscore) or  (hyphen).
	Its reg ex is: [a-zA-Z0-9]$
	 */
	
    public static final String PATTERN = 
            "^[a-zA-Z][a-zA-Z0-9._-]{0,15}[a-zA-Z0-9]$";
    public static final Pattern p = Pattern.compile(PATTERN);

	public static boolean apply(String userName) {
		Matcher matcher = p.matcher(userName);
		return matcher.find();
	}	
}
