package org.bitbucket.rajibpsarma.regex;

import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/java-regex/problem
 * @author RSarma
 *
 */
public class IPAddressValidator {
	/**
	 * 
	 * https://www.hackerrank.com/challenges/java-regex/problem
	 */
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }
	}
}

class MyRegex {
	public String pattern = "[0-9]{1,3}(.) {1}";
}
