package org.bitbucket.rajibpsarma.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import junit.framework.Assert;

public class RegEx2Test {
	@Test
	public void testInvalidValues() {
		// Must contain a digit from 0-9
		Assert.assertFalse(RegEx2.apply("asdfGh")); 
		
		// Must contain one uppercase character.
		Assert.assertFalse(RegEx2.apply("asdfgh")); 
		Assert.assertFalse(RegEx2.apply("asdfh12")); 
		
		// Must contain one lowercase character.
		Assert.assertFalse(RegEx2.apply("ASDFGH")); 
		Assert.assertFalse(RegEx2.apply("SDFGH65")); 
		
		// The length must be between 6 to 15 characters long
		Assert.assertFalse(RegEx2.apply("0Az"));	// Less than 6 chars
		Assert.assertFalse(RegEx2.apply("0Az2"));	// Less than 6 chars
		Assert.assertFalse(RegEx2.apply("0Az5f"));	// Less than 6 chars
		//Assert.assertFalse(RegEx2.apply("0Az5fdjhgfdsaerd"));	// More than 15 chars
	}
	
	@Test
	public void testValidValues() {
		Assert.assertTrue(RegEx2.apply("0Aztre")); // 6 chars
		Assert.assertTrue(RegEx2.apply("0Az!@#$%^"));
		Assert.assertTrue(RegEx2.apply("0Az&*()_+-="));
		Assert.assertTrue(RegEx2.apply("0Aztre433rereer")); // 15 chars
	}
}

class RegEx2 {
	/*
	 The rules to be used
	� Must contain a digit from 0-9. [0-9] {1,}
	� Must contain one uppercase character. [A-Z] {1,}
	� Must contain one lowercase character. [a-z] {1,}
	� The length must be between 6 to 15 characters long. .{3,12}
	 */
	
    public static final String PATTERN = 
            "[0-9]{1,}[A-Z]{1,}[a-z]{1,}.{3,12}";
    		//"^.{3,12}$[0-9]{1,}[A-Z]{1,}[a-z]{1,}";
    		//"([0-9]{1,}[A-Z]{1,}[a-z]{1,}.*){3,12}";
    		//"[0-9]{1,}[A-Z]{1,}[a-z]{1,}([a-z][A-Z]){3,12}";
    		
    public static final Pattern p = Pattern.compile(PATTERN);

	public static boolean apply(String userName) {
		Matcher matcher = p.matcher(userName);
		return matcher.find();
	}	
}