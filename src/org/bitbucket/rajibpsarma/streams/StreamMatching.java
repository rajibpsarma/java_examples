package org.bitbucket.rajibpsarma.streams;

import java.util.ArrayList;
import java.util.List;

public class StreamMatching {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("Rajib Sarma");
		list.add("Sanjib Sarma");
		list.add("Sanjib Sah");
		
		System.out.println(list.stream().anyMatch(elem -> elem.contains("Sarma"))); // true
		System.out.println(list.stream().anyMatch(elem -> elem.contains("Sanjib"))); // true
		System.out.println(list.stream().allMatch(elem -> elem.contains("Sanjib"))); // false
		System.out.println(list.stream().allMatch(elem -> elem.contains("jib"))); // true
		System.out.println(list.stream().noneMatch(elem -> elem.contains("jib"))); // false
	}
}
