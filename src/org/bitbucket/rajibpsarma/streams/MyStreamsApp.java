package org.bitbucket.rajibpsarma.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
It outputs the following to console:

	Filtering all Females : [{Prerana Sarma, 15 years, Female}, {Asha Sarma, 35 years, Female}]
	
	The names containing string 'Raj' are : 
	Rajib
	Raju
	
	Persons sorted on age : 
	[{Dhyan Sarma, 3 years, Male}, {Prerana Sarma, 15 years, Female}, {Rajib Sarma, 20 years, Male}, {Asha Sarma, 35 years, Female}, {Barada Sarma, 40 years, Male}]
	
	Persons sorted on age (another way) : 
	{Dhyan Sarma, 3 years, Male}
	{Prerana Sarma, 15 years, Female}
	{Rajib Sarma, 20 years, Male}
	{Asha Sarma, 35 years, Female}
	{Barada Sarma, 40 years, Male}
	
	Person with Max age : Optional[{Barada Sarma, 40 years, Male}]
	
	Person with Max age, another way : {Barada Sarma, 40 years, Male}
	
	Person with Min age : Optional[{Dhyan Sarma, 3 years, Male}]
	
	Person with Min age, another way : {Dhyan Sarma, 3 years, Male}
	
	Grouped by Gender : 
	Gender : Female
	[{Prerana Sarma, 15 years, Female}, {Asha Sarma, 35 years, Female}]
	Gender : Male
	[{Rajib Sarma, 20 years, Male}, {Dhyan Sarma, 3 years, Male}, {Barada Sarma, 40 years, Male}]
	
	The distinct strings are : 
	[a, b, c, d]
	
	Distinct Person ...
	{Rajib Sarma, 20 years, Male}
	{Prerana Sarma, 15 years, Female}
	
	Distinct strings : {1=[a, d], 2=[bb, ee], 3=[ccc, fff]}
	
	Females sorted on Age : 
	{Prerana Sarma, 15 years, Female}
	{Asha Sarma, 35 years, Female}

  @author RSarma
 
 */
public class MyStreamsApp {

	public static void main(String[] args) {
		List<Person> allPeople = getAllPeople();
		
		// Filter all females using streams
		// filter(boolean value); false ones are filtered out
		List<Person> females = allPeople.stream()
			.filter((person) -> {
				return person.getGender().equals(GENDER.FEMALE);
			})
			.collect(Collectors.toList());
		System.out.println("Filtering all Females : " + females);
		
		// Filter names based on criteria
		String filterCriteria = "Raj";
		System.out.println("\nThe names containing string '" + filterCriteria + "' are : ");
		String[] names = {"Rajib", "Raju", "Rantu", "Ranju"};
		List<String> list = Arrays.asList(names);
		list.stream().filter((name) -> {
			return name.contains(filterCriteria);
		}).forEach(name -> System.out.println(name));
		
		// Sort based on age
		List<Person> sortedList = allPeople.stream()
			.sorted(new Comparator<Person>() {
				@Override
				public int compare(Person o1, Person o2) {
					return o1.getAge().compareTo(o2.getAge());
				}
			})
			.collect(Collectors.toList());
		System.out.println("\nPersons sorted on age : ");
		System.out.println(sortedList);
		
		
		// sort based on name
		System.out.println("\nPersons sorted on age (another way) : ");
		allPeople.stream().sorted(Comparator.comparing(Person::getAge)).forEach(person -> {
			System.out.println(person);
		});
		
		
		// Find out the person with Max age
		Optional<Person> maxAge = allPeople.stream()
			.max(new Comparator<Person>() {
				@Override
				public int compare(Person o1, Person o2) {
					return o1.getAge().compareTo(o2.getAge());
				}
			});
		System.out.println("\nPerson with Max age : " + maxAge);
		
		// Another way of finding out max
		Person pMaxAge = allPeople.stream().max(Comparator.comparing(Person::getAge)).get();
		System.out.println("\nPerson with Max age, another way : " + pMaxAge);

		// Find out the person with Min age
		Optional<Person> minAge = allPeople.stream()
			.min(new Comparator<Person>() {
				@Override
				public int compare(Person o1, Person o2) {
					return o1.getAge().compareTo(o2.getAge());
				}
			});
		System.out.println("\nPerson with Min age : " + minAge);
		
		// another way of finding minimum
		Person pMinAge = allPeople.stream().min(Comparator.comparing(Person::getAge)).get();
		System.out.println("\nPerson with Min age, another way : " + pMinAge);
		
		// Group by gender
		Map<GENDER, List<Person>> map = allPeople.stream()
			.collect(Collectors.groupingBy(Person:: getGender));
		System.out.println("\nGrouped by Gender : ");
		map.forEach((gender, people) -> {
			System.out.println("Gender : " + gender);
			System.out.println(people);
		});
		
		// Determine distinct strings
		String[] strArr = {"a", "b", "c", "a", "c", "d"};
		List<String> distinctStr = Arrays.stream(strArr).distinct().collect(Collectors.toList());
		System.out.println("\nThe distinct strings are : ");
		System.out.println(distinctStr);
		
		// Determine distinct persons.
		System.out.println("\nDistinct Person ...");
		allPeople.stream().distinct().forEach(person -> {
			System.out.println(person);
		});
		
		// Group by length of string
		String[] arr = {"a", "bb", "ccc", "d", "ee", "fff"};
		list = Arrays.asList(arr);
		Map<Integer, List<String>> map1 = list.stream().collect(Collectors.groupingBy(String::length, Collectors.toList()));
		System.out.println("\nDistinct strings : " + map1);
		
		// we can also combine operations.
		// e.g. display the females sort by age
		// i.e. filter by gender and sort by age
		System.out.println("\nFemales sorted on Age : ");
		allPeople.stream()
			.filter(person -> person.getGender().equals(GENDER.FEMALE))
			.sorted(Comparator.comparing(Person::getAge))
			.forEach(female -> System.out.println(female));
	}

	private static List<Person> getAllPeople() {
		List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Rajib Sarma", 20, GENDER.MALE));
		persons.add(new Person("Prerana Sarma", 15, GENDER.FEMALE));
		persons.add(new Person("Dhyan Sarma", 3, GENDER.MALE));
		persons.add(new Person("Barada Sarma", 40, GENDER.MALE));
		persons.add(new Person("Asha Sarma", 35, GENDER.FEMALE));
		return persons;
	}
}

enum GENDER {
	MALE("Male"), FEMALE("Female");
	private String val;
	private GENDER(String gender) {
		val = gender;
	}
	public String toString() {
		return val;
	}
}

class Person {
	private String name;
	private int age;
	private GENDER gender;
	public Person(String name, int age, GENDER gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	public String toString() {
		return "{" + name + ", " + age + " years, " + gender + "}";
	}
	public GENDER getGender() {
		return gender;
	}
	public Integer getAge() { 
		return age; 
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (gender != other.gender)
			return false;
		return true;
	}
	
}