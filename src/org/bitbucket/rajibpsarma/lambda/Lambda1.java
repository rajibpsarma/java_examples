package org.bitbucket.rajibpsarma.lambda;

public class Lambda1 {
	static void m1(MathI m) {
		int a = 2;
		int b = 3;
		int c = m.add(a, b);
		System.out.println(a + " + " + b + " = " + c);
	}
	
	public static void main(String[] args) {
		/*
		MathI m = (int x, int y) -> {
			return (x+y);
		};
		*/
		MathI m = (int x, int y) -> {
			return Math.addExact(x, y);
		};
		m1(m);
		
		// Using method reference to add 2 numbers
		MathI m1 = Math :: addExact;
		m1(m1);
	}
}

interface MathI {
	public int add(int no1, int no2);
}