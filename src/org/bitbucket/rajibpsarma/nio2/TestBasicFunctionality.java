package org.bitbucket.rajibpsarma.nio2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class TestBasicFunctionality {
	private final static String FILE_NAME = "D:\\temp\\nio2.txt";
	
	@Test
	public void TestBasic() throws IOException {
		Path p = Paths.get(FILE_NAME);
		assertFalse(Files.exists(p));
		
		// Create the file
		Files.createFile(p);
		assertTrue(Files.exists(p));
		
		assertTrue(Files.isWritable(p));
		
		Files.delete(p);
		assertFalse(Files.exists(p));
	}
}
