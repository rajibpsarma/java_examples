package org.bitbucket.rajibpsarma.basics;

import java.util.Scanner;

public class StringExample {
	public static void main(String[] args) {
		//CheckPalindromeWord();
		extractTagContent();
		//checkTokens();
		/*
		String str = "https://howtodoinjava.com/java-initerview-questions";
		String[] arr = str.split("[//.]");
		System.out.println(arr.length);
		*/
	}
	
	/**
	 * https://www.hackerrank.com/challenges/java-string-tokens/problem
	 */
	private static void checkTokens() {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        
        String pattern = "[ !,?._'@]+";
        String[] arr = s.split(pattern);
        System.out.println(arr.length);
        for(int i = 0; i < arr.length; i++) {
        	System.out.println(arr[i]);
        }
        
        scan.close();
	}
	
	/**
	 * Problem is here:
	 * https://www.hackerrank.com/challenges/java-string-reverse/problem
	 */
	private static void CheckPalindromeWord() {
        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        // reverse the string
        String reverseStr = new StringBuffer(A).reverse().toString();
        if(A.equalsIgnoreCase(reverseStr)) {
        	System.out.println("Yes");
        }
        else {
        	System.out.println("No");
        }
	}
	
	// https://www.hackerrank.com/challenges/tag-content-extractor/problem
	private static void extractTagContent() {
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
		while(testCases>0){
			String line = in.nextLine();
//			System.out.println(line);
			
			int startingFrom = 0;
			boolean tagFound = false;
			
			while(startingFrom < line.length())
			{
//				System.out.println("startingFrom = " + startingFrom);
	          	// Check if a tag exists
				int posTagStart = line.indexOf("<", startingFrom);
//				System.out.println("posTagStart:"+posTagStart);
				if(posTagStart > -1) {
					int posTagEnd = line.indexOf(">", posTagStart);
//					System.out.println("posTagEnd :" + posTagEnd);
					if(posTagEnd > -1) {
						// found, check if it's </..>
						String tagName = line.substring(posTagStart+1, posTagEnd); 
//						System.out.println("tagName : " + tagName);
						if(tagName.startsWith("/")) {
							// Not valid tag
							startingFrom = posTagEnd;
							continue;
						}
						else {
//							System.out.println("valid start tag found");
							// a valid tag, find it's end tag
							String endTagName = "</" + tagName + ">";
							int posTagEnd1 = line.indexOf(endTagName, posTagEnd);
//							System.out.println("posTagEnd1 : " + posTagEnd1);
							if(posTagEnd1 > -1) {
								String content = line.substring(posTagEnd+1, posTagEnd1);
								System.out.println(content);
								startingFrom = posTagEnd1 + endTagName.length();
								tagFound = true;
								continue;
							}
							else {
								startingFrom = posTagEnd1 + endTagName.length();
								continue;
							}
						}
					}
					else {
						// No tag
						break;
					}
				}
				else {
					// no tag
					break;
				}
			}
			if(!tagFound) {
				System.out.println("None");
			}
			
			
			
			
			testCases--;
		}
	}
}
