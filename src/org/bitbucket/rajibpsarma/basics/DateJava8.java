package org.bitbucket.rajibpsarma.basics;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

public class DateJava8 {
	public static void main(String[] args) {
		determineYourAge();
	}
	
	private static void determineYourAge() {
		String dob = "1976-10-30"; // YYYY-MM-DD
		Calendar cal = Calendar.getInstance();
		String today = cal.get(Calendar.YEAR) + "-" + 
				getVal(cal.get(Calendar.MONTH)+1) + "-" + getVal(cal.get(Calendar.DAY_OF_MONTH));
		System.out.println("Today is : " + today);
		Period period = Period.between(LocalDate.parse(dob), LocalDate.parse(today));
		System.out.println("Your Age is : " + period.getYears() + " Years " +
				period.getMonths() + " Months " +
				period.getDays() + " Days");
	}
	
	private static String getVal(int val) {
		if(val < 10) {
			return "0" + val;
		}
		return "" + val;
	}
}
