package org.bitbucket.rajibpsarma.basics;

import java.util.Scanner;

/**
 * 
 * @author RSarma
 *
 */
public class DatatypeExample {
	public static void main(String[] args) {
		determineRange();
	}
	
	/**
	 * The problem:
	 * https://www.hackerrank.com/challenges/java-datatypes/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
	 */
	private static void determineRange() {
        Scanner sc = new Scanner(System.in);
        int t=sc.nextInt();

        for(int i=0;i<t;i++)
        {
        	try {
	            long x=sc.nextLong();

                System.out.println(x+" can be fitted in:");
                if(x>=-(Math.pow(2, 8)/2) && x<=(Math.pow(2, 8)/2)-1)System.out.println("* byte");
                if(x>=-(Math.pow(2, 16)/2) && x<=(Math.pow(2, 16)/2)-1)System.out.println("* short");
                if(x>=-(Math.pow(2, 32)/2) && x<=(Math.pow(2, 32)/2)-1)System.out.println("* int");
                if(x>=-(Math.pow(2, 64)/2) && x<=(Math.pow(2, 64)/2)-1)System.out.println("* long");
        	}
        	catch(Exception e) {
        		System.out.println(sc.next()+" can't be fitted anywhere.");
        	}
        }		
	}
}
