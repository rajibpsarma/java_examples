package org.bitbucket.rajibpsarma.basics;

public class ExceptionJava7 {
	public static void main(String[] args) {
		try {
			// Wait for some time
			Thread.sleep(1000);
			
			// Try to load a class
			Class.forName("com.rajib.MyInvalidClass");
		}
		catch(InterruptedException | ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
		}

		
		
		// Another example. It produces compile time error
		/*
		try {
			m1();
			m2();
		}
		catch(MyException | Exception ex) {}
		*/
	}
	private static void m1() throws MyException {throw new MyException();}
	private static void m2() throws Exception {throw new Exception();}
}
class MyException extends Exception {}
