package org.bitbucket.rajibpsarma.basics;

import java.util.Scanner;

/**
	// The problem is here:
	// https://www.hackerrank.com/challenges/java-static-initializer-block/problem
	 
 * @author RSarma
 *
 */
public class InitializerExample {

	/*
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int B = scan.nextInt();
		int H = scan.nextInt();
		if((H > 0) && (B > 0)) {
			System.out.println(H * B);
		} else {
			System.out.println("java.lang.Exception: Breadth and height must be positive");
		}
	}
	*/
	

	private static boolean flag = false;
	private static int B = 0;
	private static int H = 0;
	
	static {
		Scanner scan = new Scanner(System.in);
		B = scan.nextInt();
		H = scan.nextInt();
		if((H > 0) && (B > 0)) {
			flag = true;
		} else {
			System.out.println("java.lang.Exception: Breadth and height must be positive");
		}
	}
	
	public static void main(String[] args){
		if(flag){
			int area=B*H;
			System.out.print(area);
		}
		
	}//end of main

}
