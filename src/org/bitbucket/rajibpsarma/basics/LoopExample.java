package org.bitbucket.rajibpsarma.basics;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author RSarma
 */
public class LoopExample {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
    	loop2();
    }
    
    /**
     * Given an integer,N , print its first 10 multiples. 
     * Each multiple N x i (where 1 <= i <= 10) should be printed on 
     * a new line in the form: N x i = result.
     */
    private static void loop1() {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        
        for(int index = 1; index <= 10; index++) {
        	System.out.println(N + " x " + index + " = " + (N*index));
        }

        scanner.close();
    }
    
    /**
     * The problem is here:
     * https://www.hackerrank.com/challenges/java-loops/problem?h_r=next-challenge&h_v=zen
     */
    private static void loop2() {
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        int result = 0;
        
        StringBuffer str = new StringBuffer();
        List<String> output = new ArrayList<String>();
        
        for(int i=0;i<t;i++){
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            
            str.delete(0, str.length());
            for(int index = 0; index < n; index++) {
            	result = a;
            	for(int inner = 0; inner <= index; inner++) {
            		result += (int)Math.pow(2, inner)*b;
            	}
            	str.append(result + " ");
            }
            // Remove the last ,
            str.deleteCharAt(str.length()-1);
            output.add(str.toString());
        }
        
        // Now, print the output
        output.forEach(outputStr -> System.out.println(outputStr));
        
        in.close();
    }
}
