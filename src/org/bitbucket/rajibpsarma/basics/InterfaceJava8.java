package org.bitbucket.rajibpsarma.basics;

public class InterfaceJava8 implements MathI {
	public static void main(String[] args) {
		System.out.println(MathI.sqr(2));
		System.out.println(new InterfaceJava8().negate(-2));
	}
}

interface MathI {
	default int negate(int a) {
		return (-a);
	}
	
	static int sqr(int a) {
		return (a*a);
	}
}