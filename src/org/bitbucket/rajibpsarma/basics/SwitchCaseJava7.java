package org.bitbucket.rajibpsarma.basics;

public class SwitchCaseJava7 {
	public static void main(String[] args) {
		stringSwitch("Two");
		stringSwitch("Zero");
	}
	
	private static void stringSwitch(String str) {
		switch(str) {
			case "One":
				System.out.println(str + ": You supplied One");
				break;
			case "Two":
				System.out.println(str + ": You supplied Two");
				break;
			default:
				System.out.println(str + ": You supplied a number other than 1 or 2");
		}
	}
}
