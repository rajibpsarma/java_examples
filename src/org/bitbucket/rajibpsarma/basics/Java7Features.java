package org.bitbucket.rajibpsarma.basics;

public class Java7Features {
	private static void numberWithUnderscores() {
		int savings = 31_00_000;
		System.out.println("My current savings : " + savings);
	}
	
	public static void main(String[] args) {
		numberWithUnderscores();
	}
}
