package org.bitbucket.rajibpsarma.basics;

public class Immutable {
	private final String val;
	
	public Immutable() {
		val = "";
	}
	
	public Immutable(String val) {
		this.val = val;
	}
	
	public String getVal() {
		return val;
	}
	
	public static void main(String[] args) {
		Immutable i = new Immutable("xyz");
		System.out.println(i.getVal());
	}
}
