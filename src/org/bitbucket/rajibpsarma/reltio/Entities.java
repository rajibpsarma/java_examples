package org.bitbucket.rajibpsarma.reltio;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * It fetches few Entities from Reltio and prints the JSON.
 */
public class Entities {
	private final static String username = "xxx";
	private final static String password = "xxx";
	// It's Base64 encoded
	private final static String clientIdentification = "xxx";
	private final static String authHeaderName = "Authorization";
	private final static String env = "xxx";
	private final static String tenant = "xxx";
	
	
	public static void main(String[] args) throws Exception {
		// Get Access Token
		String accessToken = getAccessToken();
		// Get Entity details
		readEntities(accessToken);
	}
	
	/**
	 * Fetches Access Token
	 * @return The access token
	 * @throws Exception
	 */
	private static String getAccessToken() throws Exception {
		String accessToken = null;
		
		String uri = "https://auth.reltio.com/oauth/token";
		String authHeaderValue = "Basic " + clientIdentification;
		
		List<NameValuePair> nameValuePairs = new ArrayList<>();
		nameValuePairs.add(new BasicNameValuePair("username", username));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
		
		HttpResponse response = executeApi(uri, authHeaderValue, nameValuePairs);
		
		String body = EntityUtils.toString(response.getEntity());
		JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
        
		accessToken = jsonObject.getAsJsonObject().get("access_token").getAsString();
		System.out.println("accessToken = " + accessToken);
		
		return accessToken;
	}
	
	/**
	 * Fetches 5 Entity details
	 * @param accessToken The access token
	 * @throws Exception
	 */
	private static void readEntities(String accessToken) throws Exception {
		String uri = "https://" + env + ".reltio.com/reltio/api/" + tenant + "/entities?select=uri&max=5&offset=0";
		String authHeaderValue = "Bearer " + accessToken;
		
		HttpResponse response = executeApi(uri, authHeaderValue, null);
		
		String body = EntityUtils.toString(response.getEntity());
		JsonArray arr = new Gson().fromJson(body, JsonArray.class);
        
		System.out.println("Entities Read : " + arr.toString());
	}	
	
	/**
	 * Executes an API call
	 * @param uri The url
	 * @param authHeaderValue Authorization header value
	 * @param nameValuePairs
	 * @return HttpResponse
	 * @throws Exception
	 */
	private static HttpResponse executeApi(String uri, String authHeaderValue, List<NameValuePair> nameValuePairs) throws Exception {
		URIBuilder uriBuilder = new URIBuilder(uri);
		
		if(nameValuePairs != null) {
			uriBuilder.addParameters(nameValuePairs);
		}
		
		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.addHeader(authHeaderName, authHeaderValue);
		
		HttpClientBuilder builder = HttpClientBuilder.create();
		HttpClient httpClient = builder.build();
		
		return httpClient.execute(httpGet);
	}
}
