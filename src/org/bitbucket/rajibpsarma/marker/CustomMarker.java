package org.bitbucket.rajibpsarma.marker;

import org.junit.Test;

import junit.framework.Assert;

/*
 * A Marker interface. Any object that implements this interface
 * will be allowed to delete using DeleteHandler.delete() method,
 */
interface Deletable {
}

/*
 * An exception to indicate that the object being deleted does not
 * implement Deletable
 */
class DeleteNotSupportedException extends Exception {}

// A Class that allows deletion of objects thru delete()
class DeleteHandler {
	public void delete(Object obj) throws DeleteNotSupportedException {
		if(!(obj instanceof Deletable)) {
			throw new DeleteNotSupportedException();
		}
		System.out.println("Deleting " + obj);
	}
}

interface LivingObjects extends Deletable {
	void move();
}

class Person implements LivingObjects {
	public void move() {
		System.out.println("Moving");
	}
}

class Vehicle {}

public class CustomMarker {
	@Test
	public void testDeletion() {
		DeleteHandler handler = new DeleteHandler(); 
		
		// A Person is allowed to delete
		try {
			handler.delete(new Person()); 
			Assert.assertTrue(true);
		} catch(DeleteNotSupportedException ex) {
			Assert.fail("A Person should be allowed to delete");
		}
		
		// A Vehicle is not allowed to delete
		try {
			handler.delete(new Vehicle()); 
			Assert.fail("A Vehicle should NOT be allowed to delete");
		} catch(DeleteNotSupportedException ex) {
			Assert.assertTrue(true);
		}
	}
}
