package org.bitbucket.rajibpsarma.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/*
 * It experiments with reentrant .lock() method.
 * .lock() acquires the lock if it's not locked by other thread.
 * If locked by other thread, it waits till it's available.
 * The output of the program is, something like:
 
	m1() started by thread : t1
	m1() completed by thread : t1
	m1() started by thread : t2
	m1() completed by thread : t2
 
 * Rajib Sarma
 */
public class ReentrantLockTest1 {
	public static void main(String[] args) {
		ReentrantLockTest1 test = new ReentrantLockTest1();
		new MyThread5(test, "t1").start();
		new MyThread5(test, "t2").start();
	}
	
	private ReentrantLock lock = new ReentrantLock();
	
	public void m1(String threadName) {
		try {
			// Lock it
			lock.lock();
			System.out.println("m1() started by thread : " + threadName);
			try {
				TimeUnit.SECONDS.sleep(2);
			}catch(InterruptedException ex) {}
			System.out.println("m1() completed by thread : " + threadName);
		}
		finally {
			// Unlock it
			lock.unlock();
		}
	}
}

class MyThread5 extends Thread {
	private ReentrantLockTest1 test;
	private String threadName;
	public MyThread5(ReentrantLockTest1 test, String name) {
		this.test = test;
		this.threadName = name;
	}
	public void run() {
		test.m1(threadName);
	}
}
