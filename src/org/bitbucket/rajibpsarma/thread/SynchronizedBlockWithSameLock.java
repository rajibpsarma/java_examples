/*
 * The synchronized block can be used to lock methods
 * in different classes, using the same lock.
 * Here methods m1() of Sub1 and Sub2 are locked at
 * the same time, because we have used the same lock "MyLock".
 */
package org.bitbucket.rajibpsarma.thread;

public class SynchronizedBlockWithSameLock {
	public static void main(String[] args) {
		new MyThread(new Sub1()).start();
		new MyThread(new Sub2()).start();
	}
}

interface ISup {
	void m1();
}

class Sub1 implements ISup {
	public void m1() {
		synchronized ("MyLock") {
			System.out.println("--- Starting Sub1.m1() ---");
			for(int i = 0; i < 10; i++) {
				System.out.println("In Sub1 : " + i);
				try {
					Thread.sleep(200);
				} catch(Exception ex) {}
			}
			System.out.println("--- Ending Sub1.m1() ---");
		}
	}
}

class Sub2 implements ISup {
	public void m1() {
		synchronized ("MyLock") {
			System.out.println("--- Starting Sub2.m1() ---");
			for(int i = 0; i < 10; i++) {
				System.out.println("In Sub2 : " + i);
				try {
					Thread.sleep(200);
				} catch(Exception ex) {}
			}
			System.out.println("--- Ending Sub2.m1() ---");
		}
	}
}

class MyThread extends Thread {
	private ISup obj;
	public MyThread(ISup obj) {
		this.obj = obj;
	}
	public void run() {
		obj.m1();
	}
}