package org.bitbucket.rajibpsarma.thread;

// It shows that if different locks are used in synchronized blocks of a class,
// they are NOT locked together and threads can execute them parallelly.
public class SynchronizedBlockWithDifferentLock {
	public static void main(String[] args) {
		A1 a = new A1();
		new MyThread3(a).start();
		new MyThread4(a).start();
	}
}

class A1 {
	public void m1() {
		synchronized("abc") {
			System.out.println("starting m1() ...");
			try {
				Thread.sleep(2000);
			}catch(InterruptedException e) {}
			System.out.println("Ending m1()");			
		}
	}
	public synchronized void m2() {
		synchronized("xyz") {
			System.out.println("starting m2() ...");
			try {
				Thread.sleep(2000);
			}catch(InterruptedException e) {}
			System.out.println("Ending m2()");
		}
	}	
}

class MyThread3 extends Thread {
	private A1 a;
	public MyThread3(A1 a) {
		this.a = a;
	}
	public void run() {
		a.m1();
	}
}

class MyThread4 extends Thread {
	private A1 a;
	public MyThread4(A1 a) {
		this.a = a;
	}
	public void run() {
		a.m2();
	}
}
