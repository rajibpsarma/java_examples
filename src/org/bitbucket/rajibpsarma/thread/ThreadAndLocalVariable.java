package org.bitbucket.rajibpsarma.thread;

/**
 * This program shows that the local variables in a method are not affected,
 * when used in multi-threaded environment.
 * Here, though m1() is accessed by 2 threads at the same time, the output
 * of the program is always the following:
 
	Thread : t2 : j = 200
	Thread : t1 : j = 200
  
 * @author RSarma
 *
 */
public class ThreadAndLocalVariable {
	
	public void m1(String threadName) {
		int j = 100;
		for(int i=0; i<100; i++) {
			++j;
			
			// Show that the program is running, by printing a .
			if(threadName.equals("t1")) {
				System.out.print(".");
				if(j%50==0) {
					System.out.println("");
				}
			}
			
			try {
				Thread.sleep(50);
			} catch(Exception ex) {}
		}
		System.out.println("Thread : " + threadName + " : j = " + j);
	}
	
	public static void main(String[] args) {
		ThreadAndLocalVariable t = new ThreadAndLocalVariable();
		System.out.println("The program is running");
		new MyThread11082020(t, "t1").start();
		new MyThread11082020(t, "t2").start();
	}
}

class MyThread11082020 extends Thread {
	private ThreadAndLocalVariable t;
	private String threadName;
	
	public MyThread11082020(ThreadAndLocalVariable t, String threadName) {
		this.t = t;
		this.threadName = threadName;
	}

	public void run() {
		t.m1(threadName);
	}	
}