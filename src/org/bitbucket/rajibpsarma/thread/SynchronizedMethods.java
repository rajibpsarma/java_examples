package org.bitbucket.rajibpsarma.thread;

// It shows that all the synchronized methods of a class
// are locked together and only one thread can use
// one of them at a time.
public class SynchronizedMethods {
	public static void main(String[] args) {
		A a = new A();
		new MyThread1(a).start();
		new MyThread2(a).start();
	}
}

class A {
	public synchronized void m1() {
		System.out.println("starting m1() ...");
		try {
			Thread.sleep(2000);
		}catch(InterruptedException e) {}
		System.out.println("Ending m1()");
	}
	public synchronized void m2() {
		System.out.println("starting m2() ...");
		try {
			Thread.sleep(2000);
		}catch(InterruptedException e) {}
		System.out.println("Ending m2()");
	}	
}

class MyThread1 extends Thread {
	private A a;
	public MyThread1(A a) {
		this.a = a;
	}
	public void run() {
		a.m1();
	}
}

class MyThread2 extends Thread {
	private A a;
	public MyThread2(A a) {
		this.a = a;
	}
	public void run() {
		a.m2();
	}
}
