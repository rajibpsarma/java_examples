package org.bitbucket.rajibpsarma.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/*
 * It experiments with reentrant .tryLock() method.
 * .tryLock() acquires the lock if it's not locked by other thread and returns "true".
 * If locked by other thread, it does not waits till it's available and returns "false".
 * The output of the program is, something like:
 
	m1() started by thread : t1
	m1() completed by thread : t1
 
 * Rajib Sarma
 */
public class ReentrantLockTest2 {
	public static void main(String[] args) {
		ReentrantLockTest2 test = new ReentrantLockTest2();
		new MyThread6(test, "t1").start();
		new MyThread6(test, "t2").start();
	}
	
	private ReentrantLock lock = new ReentrantLock();
	
	public void m1(String threadName) {
		boolean locked = false;
		try {
			// Lock it
			locked = lock.tryLock();
			if(locked) {
				System.out.println("m1() started by thread : " + threadName);
				try {
					TimeUnit.SECONDS.sleep(2);
				}catch(InterruptedException ex) {}
				System.out.println("m1() completed by thread : " + threadName);
			}
		}
		finally {
			// Unlock it
			if(locked) {
				lock.unlock();
			}
		}
	}
}

class MyThread6 extends Thread {
	private ReentrantLockTest2 test;
	private String threadName;
	public MyThread6(ReentrantLockTest2 test, String name) {
		this.test = test;
		this.threadName = name;
	}
	public void run() {
		test.m1(threadName);
	}
}
