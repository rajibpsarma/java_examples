package org.bitbucket.rajibpsarma.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/*
 * It experiments with reentrant lock() and unlock() methods.
 * lock() and unlock() can be called from different methods.
 * The output of the program is, something like:
 
	Locked by : t1
	m1() started by thread : t1
	m1() completed by thread : t1
	m2() started by thread : t1
	m2() completed by thread : t1
	UnLocked by : t1
	
	Locked by : t2
	m1() started by thread : t2
	m1() completed by thread : t2
	m2() started by thread : t2
	m2() completed by thread : t2
	UnLocked by : t2
 
 * Rajib Sarma
 */
public class ReentrantLockTest3 {
	public static void main(String[] args) {
		ReentrantLockTest3 test = new ReentrantLockTest3();
		new MyThread7(test, "t1").start();
		new MyThread7(test, "t2").start();
	}
	
	private ReentrantLock lock = new ReentrantLock();
	
	public void m1(String threadName) {
		// Lock it
		lock.lock();
		System.out.println("\nLocked by : " + threadName);
		System.out.println("m1() started by thread : " + threadName);
		try {
			TimeUnit.SECONDS.sleep(2);
		}catch(InterruptedException ex) {}
		System.out.println("m1() completed by thread : " + threadName);
	}
	
	public void m2(String threadName) {
		try {
			System.out.println("m2() started by thread : " + threadName);
			try {
				TimeUnit.SECONDS.sleep(2);
			}catch(InterruptedException ex) {}
			System.out.println("m2() completed by thread : " + threadName);
		}
		finally {
			// Unlock it
			System.out.println("UnLocked by : " + threadName);
			lock.unlock();
		}
	}	
}

class MyThread7 extends Thread {
	private ReentrantLockTest3 test;
	private String threadName;
	public MyThread7(ReentrantLockTest3 test, String name) {
		this.test = test;
		this.threadName = name;
	}
	public void run() {
		test.m1(threadName);
		test.m2(threadName);
	}
}
